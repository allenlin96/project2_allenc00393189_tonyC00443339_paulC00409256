// Begin code changes by Tony Huynh
package com.company;

import java.util.List;
import java.util.Random;
import java.util.concurrent.Semaphore;

public class AccessList extends Thread {
    private List<String> perms;
    Semaphore[] free_space_sem;
    private int thread_id;
    private int requests_left;
    private List<String>[] access_list;
    private int num_objects;
    private int current_domain;
    private int num_domains;
    private String[] Ashura_chan = {
            "\nHey, you really don’t get it",
            "\nEveryone‘s obsessed with the world held in their hands, learning how to tame a monster\n" +
                    "You’d sell your own tongue if that would get you chosen, digging as many graves as you’ve been betrayed",
            "\nBrainless head trying its hardest, cringe-worthy words overflow\n" +
                    "If bye-byes can be said to such a facade, I can turn into Asura",
            "\nRun away, escape, escape, escape As Buddha\n" +
                    "I can fly, step, step, step As you wish\n" +
                    "Understand, understand, even the Devil can be devoured",
            "\nCheat, sneak, sneak, sneak. In a storm\n" +
                    "Prance, skip, skip, skip. Oh really, who cares?\n" +
                    "Understand, understand, might as well bleed them dry/",
            "\nHey, you really don’t get it",
            "\nBurning the oozing tidbits of everyday life and basking in its warmth\n" +
                    "He who defied obligation has his four limbs torn apart\n" +
                    "Finding worth in an aesthetic value without comprehending\n" +
                    "Couldn’t be happier with a compulsion akin to an artificial lure",
            "\nA hilarious full course meal of pontifications and empty talk, showed superficial affection\n" +
                    "If I have to dance with a good-for-nothing priest, I’d rather hold the Devil’s hand",
            "\nRun away, escape, escape, escape As Buddha\n" +
                    "I can fly, step, step, step As you wish\n" +
                    "Understand, understand, locking on and won’t let go until the ends of Avici Hell",
            "\nCheat, sneak, sneak, sneak In a storm\n" +
                    "Prance, skip, skip, skip Oh really, who cares？\n" +
                    "Understand, understand, save your silly talk for the afterlife",
            "\nRun away, escape, escape, escape As Buddha\n" +
                    "I can fly, step, step, step As you wish\n" +
                    "Understand, understand, even the Devil can be devoured",
            "\nCheat, sneak, sneak, sneak In a storm\n" +
                    "Prance, skip, skip, skip Oh really, who cares?\n" +
                    "Understand, understand, might as well bleed them dry",
            "\nHey, squawking so loudly, stop bowing your head to everyone",
    };
    public AccessList(Semaphore[] free_space_sem, int thread_id, int requests_left, List<String>[] access_list, int num_objects, int num_domains) {
        this.free_space_sem = free_space_sem;
        this.thread_id = thread_id;
        this.requests_left = requests_left;
        this.access_list = access_list;
        this.num_objects = num_objects;
        this.num_domains = num_domains;
        if (object_contents == null) {
            object_contents = new String[num_objects];
        }
        this.current_domain = thread_id;
        this.perms = access_list[num_objects+thread_id];
    }

    @Override
    public void run() {
        Random rand = new Random();
        int index;
        int write;
        int quote;

        int yield_amount;
        //System.out.println(thread_id + " | D" + current_domain + " : " + perms);
        try {
            for (int i = 0; i < requests_left; i++) {
                index = rand.nextInt(num_domains + num_objects);
                // Objects
                yield_amount = rand.nextInt(5)+3;
                if (index < num_objects) {
                    write = rand.nextInt(2);
                    // Read
                    if (write == 0) {
                        System.out.println("[Thread: " + thread_id + "(D" + current_domain + ")] Attempting to read resource: F" + index);
                        // Arbitrator
                        if (access_list[index].contains("D"+current_domain+":R") || access_list[index].contains("D"+current_domain+":R/W")) {
                            free_space_sem[index].acquire();
                            if (object_contents[index] != null) {
                                System.out.println("[Thread: " + thread_id + "(D" + current_domain + ")] Reading" + object_contents[index] + "\n--from resource: F" + index);
                            } else {
                                System.out.println("[Thread: " + thread_id + "(D" + current_domain + ")] Reading \n--from resource: F" + index);
                            }
                            System.out.println("[Thread: " + thread_id + "(D" + current_domain + ")] yielding "+ yield_amount +" times");
                            Thread.sleep(yield_amount*50);
                            free_space_sem[index].release();
                        }
                        else {
                            System.out.println("[Thread: " + thread_id + "(D" + current_domain + ")] Operation failed, permission denied");
                            Thread.sleep(yield_amount*50);
                        }
                    }
                    // Write
                    else {
                        System.out.println("[Thread: " + thread_id + "(D" + current_domain + ")] Attempting to write resource: F" + index);

                        // Arbitrator
                        if (access_list[index].contains("D"+current_domain+":W") || access_list[index].contains("D"+current_domain+":R/W")) {
                            free_space_sem[index].acquire();
                            quote = rand.nextInt(Ashura_chan.length);
                            System.out.println("[Thread: " + thread_id + "(D" + current_domain + ")] Writing: " + Ashura_chan[quote] + "\n--to resource: F" + index);
                            object_contents[index] = Ashura_chan[quote];
                            System.out.println("[Thread: " + thread_id + "(D" + current_domain + ")] yielding "+ yield_amount +" times");
                            Thread.sleep(yield_amount*50);
                            free_space_sem[index].release();
                        }
                        else {
                            System.out.println("[Thread: " + thread_id + "(D" + current_domain + ")] Operation failed, permission denied");
                            Thread.sleep(yield_amount*50);
                        }
                    }
                }
                // Domains
                else {
                    while (index == current_domain + num_objects || index < num_objects) {
                        index = rand.nextInt((num_domains + num_objects));
                    }
                    System.out.println("[Thread: " + thread_id + "(D" + current_domain + ")] Attempting to switch from D" + current_domain + " to D" + (index - num_objects));
                    // Arbitrator
                    if (access_list[index].contains("D"+current_domain+":allow")) {
                        current_domain = index - num_objects;
                        System.out.println("[Thread: " + thread_id + "(D" + current_domain + ")] Switched to D" + (index - num_objects));
                        System.out.println("[Thread: " + thread_id + "(D" + current_domain + ")] yielding "+ yield_amount +" times");
                    }
                    else {
                        System.out.println("[Thread: " + thread_id + "(D" + current_domain + ")] Operation failed, permission denied");
                    }
                    Thread.sleep(yield_amount*50);
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    static String[] object_contents;
}
// End code changes by Tony Huynh
