// Start code changes by Paul Tran
package com.company;

public class CapabilityList extends Thread {

    private String thread;
    private int dnum;

    public CapabilityList(int num, String name){
        this.thread = name;
        this.dnum = (num+1);

    }

    @Override
    public void run(){
        for(int i = 0;i < 5;i++){
            int y = CLShare.Yield();

            switch(CLShare.RWorSwi()){
                // 0 = RW, 1 = Switch
                case(0):
                    int F = CLShare.ranF();
                    int RW = CLShare.ranRW();
                    if(RW == 0) {
                        System.out.println("[" + this.thread + " D(" + this.dnum + ")]: Trying to Read from F"+(F+1));
                        if((CLShare.capability_list[(this.dnum-1)][F] == 1 || CLShare.capability_list[(this.dnum-1)][F] == 3)){

                            try {
                                CLShare.check[F].acquire();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            System.out.println("[" + this.thread + " D(" + this.dnum + ")]: Is now reading from F"+(F+1));
                            System.out.println("[" + this.thread + " D(" + this.dnum + ")]: Yielding "+y+" times");
                            for(int j = 0; j<y; j++){

                                try {
                                    Thread.sleep(100);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                            }
                            CLShare.check[F].release();
                            System.out.println("[" + this.thread + " D(" + this.dnum + ")]: Operation complete");
                        }
                        else{
                            System.out.println("[" + this.thread + " D(" + this.dnum + ")]: Is unable to read from F"+(F+1));
                            try {
                                Thread.sleep(1);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    else{
                        System.out.println("[" + this.thread + " D(" + this.dnum + ")]: Trying to Write from F"+(F+1));
                        if((CLShare.capability_list[this.dnum-1][F] == 2 || CLShare.capability_list[this.dnum-1][F] == 3)){

                            try {
                                CLShare.check[F].acquire();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            System.out.println("[" + this.thread + " D(" + this.dnum + ")]: Is now writing from F"+(F+1));
                            System.out.println("[" + this.thread + " D(" + this.dnum + ")]: Yielding "+y+" times");

                            for(int j = 0; j<y; j++){

                                try {
                                    Thread.sleep(100);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                            }
                            System.out.println("[" + this.thread + " D(" + this.dnum + ")]: Operation complete");
                            CLShare.check[F].release();

                        }
                        else{
                            System.out.println("[" + this.thread + " D(" + this.dnum + ")]: Is unable to write from F"+(F+1));

                            try {
                                Thread.sleep(1);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                case(1):
                    int D = (CLShare.ranD()+1);
                    while(true){
                        if(D == (this.dnum)){
                            D = (CLShare.ranD()+1);
                        }
                        else{
                            break;
                        }
                    }
                    System.out.println("[" + this.thread + " D(" + this.dnum + ")]: Attempting to switch from D"+this.dnum+" to D"+D);
                    if(CLShare.capability_list[(this.dnum-1)][CLShare.obj+D-1] == 0){
                        System.out.println("[" + this.thread + " D(" + this.dnum + ")]: Operation failed, permission denied.");
                        try {
                            Thread.sleep(1);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    else{
                        this.dnum = D;
                        System.out.println("[" + this.thread + " D(" + this.dnum + ")]: Switched to D"+D);
                        System.out.println("[" + this.thread + " D(" + this.dnum + ")]: Yielding "+y+" times.");
                        for(int j = 0; j<y; j++){

                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                        }
                    }
            }

        }
    }
}
// End code changes by Paul Tran