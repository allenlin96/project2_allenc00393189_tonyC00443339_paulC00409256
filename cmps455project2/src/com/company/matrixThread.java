// Start code by Allen Lin
package com.company;

public class matrixThread extends Thread{
    String threadName;
    int domain;
    matrixThread(String threadName, int domain){
        super(threadName);
        this.threadName = threadName;
        this.domain = domain;
    }

    public void write(int action){
        System.out.println("[" + threadName + " (D" + (domain + 1) + ")]" + "writing to resource: F"+ (action + 1));
    }

    public void read(int action){
        System.out.println("[" + threadName + " (D" + (domain + 1) + ")]" + "read resource: F"+ (action + 1));
    }
    @Override
    public void run() {
        for(int i = 0; i < 5; i++){
            try{
                int col_action = Shared.getActionPerm(); //number between 0 to number of object+domains | X in example output
                while((col_action - Shared.objects)== domain){
                    col_action = Shared.getActionPerm();
                }
                int temp = Shared.getRandomYield(); // random amount to yield for
                if(col_action < Shared.objects){ //read or write col_action is in the range of objects
                    int read_write = Shared.getReadOrWrite(); // generate a random number either 0 or 1. 0 is read; 1 is write.
                    if(read_write == 0){ //try to read
                        System.out.println("[" + threadName + " (D" + (domain + 1) + ")]" + "Attempting to read resource: F" + (col_action + 1) );
                        System.out.println("[" + threadName + " (D" + (domain + 1) + ")]" + "Yielding " + temp + " times");
                        Thread.sleep(temp * 50);
                        System.out.println("[" + threadName + " (D" + (domain + 1) + ")]" + "Checking with arbitrator function");
                        if(arbitrator("R", domain, col_action)){
                            System.out.println("[" + threadName + " (D" + (domain + 1) + ")]" + "Permission to read resource: F"+ (col_action + 1) + " granted");
                            read(col_action);
                            System.out.println("[" + threadName + " (D" + (domain + 1) + ")]" + "Yielding " + temp + " times");
                            Thread.sleep(temp * 50);
                            Shared.locks[col_action].release();
                            System.out.println("[" + threadName + " (D" + (domain + 1) + ")]" + "Operation Completed");
                        }
                        else {
                            System.out.println("[" + threadName + " (D" + (domain + 1) + ")]" + "Permission to read resource: F" + (col_action + 1)+ " not granted");
                            System.out.println("[" + threadName + " (D" + (domain + 1) + ")]" + "Yielding " + temp + " times");
                            Thread.sleep(temp * 50);
                            System.out.println("[" + threadName + " (D" + (domain + 1) + ")]" + "Operation Completed");
                        }
                    }
                    else{ // try to write
                        System.out.println("[" + threadName + " (D" + (domain + 1) + ")]" + "Attempted to write to resource: F" + (col_action + 1));
                        System.out.println("[" + threadName + " (D" + (domain + 1) + ")]" + "Yielding " + temp + " times");
                        Thread.sleep(temp * 50);
                        System.out.println("[" + threadName + " (D" + (domain + 1) + ")]" + "Checking with arbitrator function");
                        if(arbitrator("W", domain, col_action)){
                            System.out.println("[" + threadName + " (D" + (domain + 1) + ")]" + "Permission to write resource: F" + (col_action + 1)+ " granted");
                            write(col_action);
                            System.out.println("[" + threadName + " (D" + (domain + 1) + ")]" + "Yielding " + temp + " times");
                            Thread.sleep(temp * 50);
                            Shared.locks[col_action].release();
                            System.out.println("[" + threadName + " (D" + (domain + 1) + ")]" + "Operation Completed");
                        }
                        else {
                            System.out.println("[" + threadName + " (D" + (domain + 1) + ")]" + "Permission to write resource: F" + (col_action + 1)+ " not granted");
                            System.out.println("[" + threadName + " (D" + (domain + 1) + ")]" + "Yielding " + temp + " times");
                            Thread.sleep(temp * 50);
                            System.out.println("[" + threadName + " (D" + (domain + 1) + ")]" + "Operation Completed");
                        }
                    }
                }
                else { //domain switch
                    System.out.println("[" + threadName + " (D" + (domain + 1) + ")]" + "Attempting to switch from D" + (domain + 1) + " to D" + (col_action - Shared.objects + 1) );
                    if(arbitrator("switch", domain, col_action)){
                        this.domain = (col_action - Shared.objects);
                        System.out.println("[" + threadName + " (D" + (domain + 1) + ")]" + "Switched to D" + (domain + 1));
                        Thread.sleep(100);
                    }
                    else {
                        System.out.println("[" + threadName + " (D" + (domain + 1) + ")]" + "Operation failed, permissions denied");
                        Thread.sleep(100);
                    }
                }
            }
            catch (Exception e) { e.printStackTrace();}
        }
    }
    public boolean arbitrator(String s, int row, int col){
        if(s.equals("R")){
            if(Shared.access_matrix[row][col].contains("R")){
                try {
                        Shared.locks[col].acquire();
                }
                catch(Exception e) {
                }
                return true;
            }
            else
                return false;
        }
        else if(s.equals("W")){
            if(Shared.access_matrix[row][col].contains("W")){
                try {
                        Shared.locks[col].acquire();
                }
                catch(Exception e) {
                }
                return true;
            }
            else
                return false;
        }
        else if(s.equals("switch")){
            if(Shared.access_matrix[row][col].contains("allow")){
                return true;
            }
            else
                return false;
        }
        return false;
    }
}
// End code by Allen Lin